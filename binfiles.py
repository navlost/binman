#!/bin/usr/python3

import io
import struct
from collections import namedtuple

st_header = struct.Struct(">HH2s9sc")
Header = namedtuple("Header", "id size i00 null eoh")

#st_hydronav_header = struct.Struct(">8s7xc")
#HydronavHeader = namedtuple("hydronav eoh")

st_hydronav = struct.Struct(">8s 7s c 8s 15s c 1s c 8s c 7s c 10s c 11s 1s c 11s 1s c 6s 2s c 6s 2s c 5s 1s c 10s c 5s 1s c 8s 1s c 1s c 5s 1s c 16s 1s c")
Hydronav = namedtuple("Hydronav", "hydronav i00 eoh1 uhead time i01 gps_clock i02 vessel_name i03 station_number i04 fsid i05 northings northings_suffix i06 eastings eastings_suffix i07 vessel_speed vessel_speed_suffix i08 bearing bearing_suffix i09 water_depth water_depth_suffix i10 date i11 distance_offline distance_offline_suffix i12 distance_inline distance_inline_suffix i13 line_status i14 gps_height gps_height_suffix i15 line_name eod eoh2")

st_smartsource_header = struct.Struct(">8s 2s H 3s c")
SmartsourceHeader = namedtuple("SmartsourceHeader", "id i00 size i01 eoh")

st_smartsource_gcs90_header = struct.Struct(">6s 4s 30s 10s 2s 1s 17s 1s 1s 2s 2s 2s 2s 2s 4s 6s 5s 5s 6s 4s 88s")
SmartsourceGcs90Header = namedtuple("SmartsourceGcs90Header", "header blk_siz line shot mask trg_mode time src_number num_subarray num_guns num_active num_delta num_auto num_nofire spread volume avg_delta std_delta baro_press manifold spare")

st_smartsource_gcs90_gun = struct.Struct(">1s 2s 1s 1s 1s 1s 1s 6s 6s 4s 4s 4s 4s 4s")
SmartsourceGcs90Gun = namedtuple("SmartsourceGcs90Gun", "string gun source mode detect autofire spare aim_point firetime delay depth pressure volume filltime")

st_smartsource_footer = struct.Struct(">2s c")
SmartsourceFooter = namedtuple("SmartsourceFooter", "crnl eoh")

Smartsource = namedtuple("Smartsource", "header gcs90_header records footer")

BinError = namedtuple("BinError", "offset length message")

Record = namedtuple("Record", "header hydronav smartsource")

def _check_chunk_size(chunk, size, fd):
	if len(chunk) != size:
		# print("At offset {} read {} bytes, expected {}".format(fd.tell(), len(chunk), size))
		# print(chunk)
		return False
		
	return True

def decode(path, ss_errors="skip"):

	"""Decodes a "BIN" file, as used by Equinor in PRM surveys of Snorre oilfield."""
	
	records = []
	error_pos = None
	
	with open(path, "rb") as fd:
		
		while True:
			
			pos = fd.tell()
			# print("Pos", pos)
			
			record, eof = read_record(fd, ss_errors)
			
			if record is not None:
				if error_pos is not None:
					# Append an error record
					error = BinError(error_pos, pos-error_pos, "Bad data")
					records.append(error)
					print("Resuming from", pos, pos-error_pos)
					error_pos = None
			
				records.append(record)
			elif eof is True:
				print("At EOF")
				break
			else:
				if error_pos is None:
					print("Found error at position", fd.tell(), record, eof)
					error_pos = fd.tell()
				# Move forward in increments of one byte
				# until we find the next valid record or EOF
				fd.seek(fd.tell()+1)

	return records
	
def read_record(fd, ss_errors):
	header, eof = read_header(fd)
	if eof:
		return (None, True)
	if header:
		hydronav = read_hydronav(fd)
		if hydronav:
			smartsource = read_smartsource(fd)
			if smartsource or ss_errors == "skip":
				return (Record(header, hydronav, smartsource), False)
				
	return (None, False)
	
def read_header(fd):
	pos = fd.tell()
	chunk = fd.read(st_header.size)
	if _check_chunk_size(chunk, st_header.size, fd):
		header = Header(*st_header.unpack(chunk))
		if header.id == 1793 and header.null == b"\x00" * 9 and header.eoh == b"\xff":
			return (header, False)
	elif len(chunk) == 0:
		# We have successfully reached the end of file
		return (None, True)
			
	fd.seek(pos)
	return (None, False)

def read_hydronav(fd):
	pos = fd.tell()
	chunk = fd.read(st_hydronav.size)
	if _check_chunk_size(chunk, st_hydronav.size, fd):
		hydronav = Hydronav(*st_hydronav.unpack(chunk))
		if hydronav.eoh1 == b"\xff" and hydronav.eoh2 == b"\xff" and hydronav.eod == b"*" and hydronav.hydronav == b"HYDRONAV" and hydronav.uhead == b"@@@UHEAD":
			return hydronav
			
	fd.seek(pos)
	return None

def read_smartsource(fd):
	smartsource_guns = []
	pos = fd.tell()
	
	try:
		smartsource_header = read_smartsource_header(fd)
		if smartsource_header:
			smartsource_gcs90_header = read_smartsource_gcs90_header(fd)
			if smartsource_gcs90_header:
				smartsource_bytes_left = smartsource_header.size
				smartsource_bytes_left -= st_smartsource_header.size
				smartsource_bytes_left -= st_smartsource_gcs90_header.size
				smartsource_bytes_left -= st_smartsource_footer.size
				
				while smartsource_bytes_left > 0:
					smartsource_gcs90_gun = read_smartsource_gcs90_gun(fd)
					smartsource_bytes_left -= st_smartsource_gcs90_gun.size
					# print(smartsource_gcs90_gun, smartsource_bytes_left)
					if smartsource_gcs90_gun:
						smartsource_guns.append(smartsource_gcs90_gun)
					else:
						# print("smartsource_gcs90_gun error", smartsource_gcs90_gun, smartsource_bytes_left)
						raise ValueError("Corrupted Smartsource record")
				
				smartsource_footer = read_smartsource_footer(fd)
				if smartsource_footer:
					smartsource = Smartsource(smartsource_header, smartsource_gcs90_header, smartsource_guns, smartsource_footer)
					return smartsource
					
	except ValueError:
		pass
		
	fd.seek(pos)
	return None
		
	
def read_smartsource_header(fd):
	pos = fd.tell()
	chunk = fd.read(st_smartsource_header.size)
	if _check_chunk_size(chunk, st_smartsource_header.size, fd):
		smartsource_header = SmartsourceHeader(*st_smartsource_header.unpack(chunk))
		if smartsource_header.id == b"SmartSou" and smartsource_header.eoh == b"\xff":
			return smartsource_header
		# else:
			# print("read_smartsource_header", smartsource_header)
		
	fd.seek(pos)
	return None

def read_smartsource_gcs90_header(fd):
	pos = fd.tell()
	chunk = fd.read(st_smartsource_gcs90_header.size)
	if _check_chunk_size(chunk, st_smartsource_gcs90_header.size, fd):
		smartsource_gcs90_header = SmartsourceGcs90Header(*st_smartsource_gcs90_header.unpack(chunk))
		if smartsource_gcs90_header.header == b"*SMSRC":
			return smartsource_gcs90_header
		# else:
			# print("smartsource_gcs90_header", smartsource_gcs90_header)
		
	fd.seek(pos)
	return None


def read_smartsource_gcs90_gun(fd):
	pos = fd.tell()
	chunk = fd.read(st_smartsource_gcs90_gun.size)
	if _check_chunk_size(chunk, st_smartsource_gcs90_gun.size, fd):
		return SmartsourceGcs90Gun(*st_smartsource_gcs90_gun.unpack(chunk))
		
	fd.seek(pos)
	return None
		
def read_smartsource_footer(fd):
	pos = fd.tell()
	chunk = fd.read(st_smartsource_footer.size)
	if _check_chunk_size(chunk, st_smartsource_footer.size, fd):
		smartsource_footer = SmartsourceFooter(*st_smartsource_footer.unpack(chunk))
		if smartsource_footer.crnl == b"\r\n" and smartsource_footer.eoh == b"\xff":
			return smartsource_footer
			
	fd.seek(pos)
	return None
	

def read_hdr(path):
	smartsource_records = []
	
	with open(path, "rb") as fd:
		
		# smartsource_header = SmartsourceHeaer(*st_smartsource_header.unpack(b"SmartSou\x01\x01\x00\x00\x01\x00\x00\xff"))
		smartsource_gcs90_header = None
		smartsource_footer = None
		while True:
		
			byte_stream = io.BytesIO(fd.readline())
			if not byte_stream:
				break
				
			smartsource_guns = []
			smartsource_gcs90_header = read_smartsource_gcs90_header(fd)
			if not smartsource_gcs90_header:
				break
			
			blk_siz = int(smartsource_gcs90_header.blk_siz.decode("ascii"))
			num_guns = (blk_siz - st_smartsource_gcs90_header.size) // st_smartsource_gcs90_gun.size
			for gun in range(num_guns):
				smartsource_gcs90_gun = read_smartsource_gcs90_gun(fd)
				if smartsource_gcs90_gun:
					smartsource_guns.append(smartsource_gcs90_gun)
			
			smartsource_record_size = st_smartsource_header.size + st_smartsource_gcs90_header.size + st_smartsource_gcs90_gun.size * len(smartsource_guns) + st_smartsource_footer.size
			smartsource_header = SmartsourceHeader(*st_smartsource_header.unpack(st_smartsource_header.pack(b"SmartSou", b"\x00\x00", smartsource_record_size, b"\x00\x00\x00", b"\xff")))
			smartsource_footer = SmartsourceFooter(*st_smartsource_footer.unpack(st_smartsource_footer.pack(b"\r\n", b"\xff")))
			smartsource = Smartsource(smartsource_header, smartsource_gcs90_header, smartsource_guns, smartsource_footer)
			smartsource_records.append(smartsource)
			
	return smartsource_records
	
def merge_hdr(bin_records, hdr_records):
	
	output_records = []
	hdr_dict = dict()
	for hdr in hdr_records:
		hdr_dict[int(hdr.gcs90_header.shot.decode("ascii"))] = hdr
		
	for bin in bin_records:
		shotpoint = int(bin.hydronav.station_number.decode("ascii"))
		
		if shotpoint in hdr_dict:
			smartsource = hdr_dict[shotpoint]
			merged_record = Record(bin.header, bin.hydronav, smartsource)
			output_records.append(merged_record)
		else:
			print("Shot", shotpoint, "not found")
			
	
	return output_records
	
def encode_record(record, correct_record_size=True):
	header, hydronav, smartsource = record

	with io.BytesIO() as buffer:
		buffer.write(st_header.pack(*header))
		buffer.write(st_hydronav.pack(*hydronav))
		buffer.write(st_smartsource_header.pack(*smartsource.header))
		buffer.write(st_smartsource_gcs90_header.pack(*smartsource.gcs90_header))
		for gun in smartsource.records:
			buffer.write(st_smartsource_gcs90_gun.pack(*gun))
		buffer.write(st_smartsource_footer.pack(*smartsource.footer))
		
		bytes = buffer.getvalue()
		if correct_record_size:
			if len(bytes) != header.size:
				print(len(bytes), "!=", header.size)
				corrected_header = Header(header.id, len(bytes), header.i00, header.null, header.eoh)
				corrected_record = Record(corrected_header, hydronav, smartsource)
				return encode_record(corrected_record)
				
		return bytes

	
def encode(path, records):
	with open(path, "wb") as fd:
		for record in records:
			fd.write(encode_record(record))
			
