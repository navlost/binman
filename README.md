# Binman

A viewer and manager for the navigation BIN files used in some permanent reservoir monitoring projects in the North Sea.

The BIN files that this application reads are a capture of a specific user header 7 setup used in Ocean Bottom Cable seismic acquisition. The application can however be generalised to read different configurations, should the need arise.

Binman decodes these files and checks for integrity. If a record cannot be decoded correctly, it flags the corrupted part of the file and tries to find the next valid record. In the case of gun controller header errors, if the user has the corresponding HDR files those can be merged into the BIN data which in many cases fixes data issues. The modified file can be saved back to disk under a different name.

## Installation

### Binaries

#### Windows / Linux

Windows users may download precompiled executables from [this repository](https://gitlab.com/navlost/binman/tags).

Linux users may prefer to download and run from source. This application requires [PyQt5](https://pypi.org/project/PyQt5/).

### Source

`git clone https://gitlab.com/navlost/binman.git`

### Support

Bug reports and feature requests should be done via the [issue tracker](https://gitlab.com/navlost/binman/issues).

For context-sensitive help, use <kbd>Shift</kbd>+<kbd>F1</kbd> (or select the “What's this…” option of the “Help” menu) and then click on the part of the user interface you want to know more about.
