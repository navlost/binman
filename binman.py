import sys
import os
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QTableWidgetItem, QHeaderView, QMessageBox, QWhatsThis
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from main_window import Ui_MainWindow
import binfiles

_version = "0.2.0"

class Binman(QMainWindow):
	
	def __init__(self):
		super().__init__()
		
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		
		self.ui.hydronavTable.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
		self.ui.smartsourceHeaderTable.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
		self.ui.smartsourceHeaderTable.horizontalHeader().setMaximumSectionSize(175)
		self.ui.smartsourceGunRecordTable.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
		
		self.ui.actionOpen.triggered.connect(self.open_file)
		self.ui.actionSaveAs.triggered.connect(self.save_as)
		self.ui.actionMergeHDRFile.triggered.connect(self.merge_hdr)
		self.ui.actionAbout.triggered.connect(self.about)
		self.ui.actionWhatsThis.triggered.connect(lambda checked: QWhatsThis.enterWhatsThisMode())
		self.ui.recordsList.currentRowChanged.connect(self.view_record)
		self.ui.recordsList.currentRowChanged.connect(self.select_gun_record)
		self.ui.smartSourceGunRecordSpinBox.valueChanged.connect(self.select_gun_record)
		
		self.filename = None
		self.records = []
		
	def about(self):
		QMessageBox.about(self, "About Binman", """
			<p>Binman is a viewer and manager for the navigation BIN files used in some permanent reservoir monitoring projects in the North Sea.</p>

			<p><table><tr><td>Version:</td><td>{}</td></tr>
			<tr><td>Homepage:</td><td><a href="https://gitlab.com/navlost/binman">https://gitlab.com/navlost/binman</a></td></tr>
			<tr><td>Support:</td><td><a href="https://gitlab.com/navlost/binman/issues">https://gitlab.com/navlost/binman/issues</a></td></tr>
			<tr><td>Licence:</td><td>GPL-3.0</td></tr></table></p>

			<p>© 2019 D. Berge</p>""".format(_version))
		
	@pyqtSlot()
	def open_file(self, filename=None):
		
		if filename is None:
			title = "Select a BIN file"
			filename, _ = QFileDialog.getOpenFileName(self, title, "", "BIN files (*.bin *.BIN);;All (*.* *)")
		if filename:
			self.filename = filename
			self.ui.statusbar.showMessage("Reading file " + filename)
			
			try:
				records = binfiles.decode(filename)
				if records:
					self.ui.statusbar.showMessage("Loaded {} records from {}".format(len(records), os.path.basename(filename)))
					self.setWindowTitle("BIN file viewer – " + os.path.basename(filename))
					self.ui.actionSaveAs.setEnabled(True)
					self.ui.actionMergeHDRFile.setEnabled(True)
					self.load_records(records)
			except FileNotFoundError:
				QMessageBox.critical(self, "File not found", "The file {} could not be found. Please check the name and try again".format(filename))
				
	def save_as(self):
		title = "Select a BIN file name to save as"
		filename, _ = QFileDialog.getSaveFileName(self, title, os.path.dirname(self.filename), "BIN files (*.bin);;All (*.* *)")
		if filename:
			self.filename = filename
			self.ui.statusbar.showMessage("Saving to " + os.path.basename(filename))
			binfiles.encode(filename, self.records)
			self.ui.statusbar.showMessage("Saved {} records to {}".format(len(self.records), os.path.basename(filename)))
			self.setWindowTitle("BIN file viewer – " + os.path.basename(filename))
			
	def merge_hdr(self):
		title = "Select an HDR file"
		filename, _ = QFileDialog.getOpenFileName(self, title, "", "HDR files (*.hdr *.HDR);;All (*.* *)")
		if filename:
			basename = os.path.basename(filename)
			self.ui.statusbar.showMessage("Reading HDR records from " + basename)
			hdr = binfiles.read_hdr(filename)
			bin = [ r for r in self.records if type(r) == binfiles.Record ]
			if hdr:
				self.ui.statusbar.showMessage("Read {} HDR records from {}. Now merging".format(len(hdr), basename))
				records = binfiles.merge_hdr(bin, hdr)
				if records:
					self.ui.statusbar.showMessage("Successfully merged {} records".format(len(records)))
					if len(records) != len(bin):
						QMessageBox.warning(self, "Merging HDR records", "Please note that {} records could not be merged successfully as there were no matching HDR records".format(len(bin) - len(records)), QMessageBox.Ok)
					else:
						QMessageBox.information(self, "Merging HDR records", "{} records successfully merged. Use Save as… to save to a new BIN file.".format(len(records)), QMessageBox.Ok)
						
					self.load_records(records)
					
				else:
					QMessageBox.warning(self, "Merging HDR records failed", "No matching records in the HDR and BIN files. Please check that you have selected the right pair of files.", QMessageBox.Ok)
			else:
				QMessageBox.warning(self, "Merging HDR records failed", "No gun records found in HDR file. Please check that you have selected the correct file.", QMessageBox.Ok)

	def clear_table_values(self, widget):
		
		print(widget.rowCount(), widget.columnCount())
		for row in range(widget.rowCount()):
			item = widget.takeItem(row, 1)
			if item:
				del item
				
	def clear_views(self):
		self.clear_table_values(self.ui.hydronavTable)
		self.clear_table_values(self.ui.smartsourceHeaderTable)
		self.clear_table_values(self.ui.smartsourceGunRecordTable)
		self.ui.smartsourceNumberOfGunRecordsSpinBox.setValue(0)
		self.ui.smartSourceGunRecordSpinBox.setMaximum(0)
		self.ui.smartSourceGunRecordSpinBox.setValue(0)
	
				
	def load_records(self, records):
		self.records = records

		self.ui.recordsList.clear()
		self.clear_views()
		
		for record in records:
			self.add_to_list(record)
			
		self.ui.recordsList.setCurrentRow(0)
			
	def add_to_list(self, record):
		w = self.ui.recordsList
		try:
			if type(record) == binfiles.Record:
				label = "R{:04d} - {} [H{}]".format(w.count(), record.hydronav.station_number.decode("ascii").strip(), "S" if record.smartsource else "")
			elif type(record) == binfiles.BinError:
				label = "R{:04d} - Corrupted ({},{})".format(w.count(), record.offset, record.length)
		except UnicodeDecodeError:
			label = "R{:04d} - (corrupted?)".format(w.count())
			
		w.addItem(label)
		
		if type(record) == binfiles.BinError:
			w.item(w.count()-1).setForeground(Qt.gray)
		
	def select_gun_record(self, gun_index):
		record_index = self.ui.recordsList.currentRow()
		if gun_index > 0 and record_index >= 0:
			if record_index < len(self.records):
				record = self.records[record_index]
				if type(record) == binfiles.Record and type(record.smartsource) == binfiles.Smartsource:
					records = self.records[record_index].smartsource.records
					if gun_index - 1 < len(records):
						gun_record = self.records[record_index].smartsource.records[gun_index-1]
						self.view_gun_data(gun_record)
						return

		self.clear_table_values(self.ui.smartsourceGunRecordTable)
		
	def view_record(self, index):
		if index >= 0:
			record = self.records[index]
			if type(record) == binfiles.Record:
				# print(record)
				self.view_hydronav(record)
				self.view_smartsource(record)
				return
				
		self.clear_views()
				
	def view_hydronav(self, record):
		h = record.hydronav._asdict() # For brevity
		
		content = (
			( 0, 1, "hydronav"),
			( 1, 1, "eoh1"),
			( 2, 1, "uhead"),
			( 3, 1, "time"),
			( 4, 1, "gps_clock"),
			( 5, 1, "vessel_name"),
			( 6, 1, "station_number"),
			( 7, 1, "fsid"),
			( 8, 1, "northings"),
			( 8, 2, "northings_suffix"),
			( 9, 1, "eastings"),
			( 9, 2, "eastings_suffix"),
			(10, 1, "vessel_speed"),
			(10, 2, "vessel_speed_suffix"),
			(11, 1, "bearing"),
			(11, 2, "bearing_suffix"),
			(12, 1, "water_depth"),
			(12, 2, "water_depth_suffix"),
			(13, 1, "date"),
			(14, 1, "distance_offline"),
			(14, 2, "distance_offline_suffix"),
			(15, 1, "distance_inline"),
			(15, 2, "distance_inline_suffix"),
			(16, 1, "line_status"),
			(17, 1, "gps_height"),
			(17, 2, "gps_height_suffix"),
			(18, 1, "line_name"),
			(19, 1, "eod"),
			(20, 1, "eoh2")
		)
		
		for datum in content:
			row, col, key = datum
			try:
				item = QTableWidgetItem(h[key].decode("ascii"))
			except UnicodeDecodeError:
				item = QTableWidgetItem(str(h[key]))
			item.setTextAlignment(Qt.AlignRight)
			self.ui.hydronavTable.setItem(row, col, item)
			
	def view_smartsource(self, record):
		if type(record.smartsource) == binfiles.Smartsource:
			guns = len(record.smartsource.records)
			self.view_gun_header(record)
			self.ui.smartsourceNumberOfGunRecordsSpinBox.setValue(guns)
			self.ui.smartSourceGunRecordSpinBox.setMinimum(1)
			self.ui.smartSourceGunRecordSpinBox.setMaximum(guns)
			# self.ui.smartSourceGunRecordSpinBox.setValue(1)
		else:
			self.clear_table_values(self.ui.smartsourceHeaderTable)
			# self.clear_table_values(self.ui.smartsourceGunRecordTable)
			
	def view_gun_header(self, record):
		h = record.smartsource.gcs90_header._asdict() # For brevity
		# "header blk_siz line shot mask trg_mode time src_number num_subarray num_guns num_active num_delta num_auto num_nofire spread volume avg_delta std_delta baro_press manifold spare"
		content = (
			( 0, 1, "header"),
			( 1, 1, "blk_siz"),
			( 2, 1, "line"),
			( 3, 1, "shot"),
			( 4, 1, "mask"),
			( 5, 1, "trg_mode"),
			( 6, 1, "time"),
			( 7, 1, "src_number"),
			( 8, 1, "num_subarray"),
			( 9, 1, "num_guns"),
			(10, 1, "num_active"),
			(11, 1, "num_delta"),
			(12, 1, "num_auto"),
			(13, 1, "num_nofire"),
			(14, 1, "spread"),
			(15, 1, "volume"),
			(16, 1, "avg_delta"),
			(17, 1, "std_delta"),
			(18, 1, "baro_press"),
			(19, 1, "manifold"),
			(20, 1, "spare")
		)

		for datum in content:
			row, col, key = datum
			try:
				item = QTableWidgetItem(h[key].decode("ascii"))
			except UnicodeDecodeError:
				item = QTableWidgetItem(str(h[key]))
			item.setTextAlignment(Qt.AlignRight)
			self.ui.smartsourceHeaderTable.setItem(row, col, item)
			
	def view_gun_data(self, gun_record):
		"string gun source mode detect autofire spare aim_point firetime delay depth pressure volume filltime"
		
		h = gun_record._asdict()
		
		content = (
			( 0, 1, "string"),
			( 1, 1, "gun"),
			( 2, 1, "source"),
			( 3, 1, "mode"),
			( 4, 1, "detect"),
			( 5, 1, "autofire"),
			( 6, 1, "spare"),
			( 7, 1, "aim_point"),
			( 8, 1, "firetime"),
			( 9, 1, "delay"),
			(10, 1, "depth"),
			(11, 1, "pressure"),
			(12, 1, "volume"),
			(13, 1, "filltime")
		)

		for datum in content:
			row, col, key = datum
			try:
				print(key, h[key])
				item = QTableWidgetItem(h[key].decode("ascii"))
			except UnicodeDecodeError:
				item = QTableWidgetItem(str(h[key]))
			item.setTextAlignment(Qt.AlignRight)
			self.ui.smartsourceGunRecordTable.setItem(row, col, item)
		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	window = Binman()
	window.show()
	if len(sys.argv) > 1:
		window.open_file(sys.argv[1])
	sys.exit(app.exec_())
